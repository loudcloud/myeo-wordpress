<?php
/**
 * The page template file
 *
 * @package Next Gen EO
 * @since 0.1.0
 */

 get_header(); ?>

    <div class="page__container">
        <?php get_template_part('the_loop'); ?>
    </div>

<?php get_footer(); ?>