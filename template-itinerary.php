<?php
/**
 * The itinerary template file
 * Template Name: Itinerary
 *
 * @package Next Gen EO
 * @since 0.1.0
 */

 get_header(); ?>

    <div class="itinerary__container">
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

            <h1 class="itinerary__header"><?php the_title(); ?></h1>
            <p>Here’s a sneak peek of what to expect!</p>
            <div class="block__container">
                <?php the_content(); ?>
            </div>

        <?php endwhile; else : ?>


            <!-- The very first "if" tested to see if there were any Posts to -->
            <!-- display.  This "else" part tells what do if there weren't any. -->
            <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>

        <!-- REALLY stop The Loop. -->
        <?php endif; ?>
    </div>
 <?php get_footer(); ?>