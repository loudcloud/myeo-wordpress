<?php
/**
 * The main template file
 *
 * @package Next Gen EO
 * @since 0.1.0
 */

 get_header(); ?>

 <?php get_template_part('the_loop'); ?>

 <?php get_footer(); ?>