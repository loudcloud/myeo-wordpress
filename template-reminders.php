<?php
/**
 * The itinerary template file
 * Template Name: Reminders
 *
 * @package Next Gen EO
 * @since 0.1.0
 */

 get_header(); ?>

    <div class="reminders__container">
      <h1 class="reminders__header">Reminders</h1>
      <!-- <p class="reminders__subheader">Coming Soon!</p>
      <img src="<?php echo get_template_directory_uri() ?>/assets/images/coming-soon.jpg" class="coming-soon-img" alt="Coming Soon" /> -->

      <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

            <div class="block__container reminders__block">
                <?#php the_content(); ?>
                <div class="block two-thirds">
                    <h1>Welcome MyEO NextGen Manila Camp, Parents!</h2>
                    <p>From April 2-5, 2015, your kids will be in the best EO hands—that is our promise and our commitment. So your precious bundles of joy—oh wait, these are teenagers! Ehem, your TEENAGERS!—are in for the ride of their life so put them front and center and give them a nudge our way.</p>
                    <p>However, we do realize that you may have a few questions regarding the Camp and whatnot, so here is a primer to help ease your mind about your child’s stay with us.</p>

                    <h2>WHAT EXACTLY IS IT AGAIN?</h2>
                    <p>MyEO NextGen Manila is a 4-day entrepreneurs camp where teenagers travel through time in a unique, once-in-a-lifetime experience in the historic city of Manila. Your next generation entrepreneurs will get the opportunity to:</p>
                    <ul>
                        <li>learn and hone exciting new skills on creativity, innovation and collaboration</li>
                        <li>develop personal leadership</li>
                        <li>make new friends from all over the globe</li>
                        <p>If you haven’t had a chance to look at our website, please visit: <a href="www.myeonextgen.com">www.myeonextgen.com</a></p>
                    </ul>

                    <h2>OH, WHAT TO BRING!</h2>
                    <p>They will be staying at the <b>Sofitel Philippine Plaza</b>, which is a 5-star hotel, so all basic hotel amenities will be available. (Nope, anti-perspirant isn’t in the set). They do need to bring clothes, of course, which would include 4 sets for daytime activities (shorts are cool!), as well as a set for physical activities such as jogging or yoga pants and loose cotton t-shirts. Don’t forget running shoes or sneakers for when they take those long walks around historic Manila.</p>
                    <p>You may want to include caps, hats or visors for when they go out under the sun, swimming attire (have you seen the pool?), and a light rain jacket in case the sun decides not to cooperate with us</p>
                    <p>We also understand that some of them may have medication or vitamins they need to take (or that you insist they do), or solutions for cleaning those contact lenses so best to pack those as well.</p>

                    <h2>OTHER STUFF</h2>
                    <p>We encourage them to bring their mobile phones (smartphones are so inexpensive now, aren’t they?) so they have a chance to document their experiences in the camp. If you worry about data charges in a foreign country, let them bring their point-and-shoot cameras instead. They can also bring small laptops and tablets, which they will have a chance to use during the camp activities.</p>
                    <p>We all love bling and shiny, expensive things, but in this case, these are best left at home. It is a camp. With luck (and a lot of fun, hard work on our part and theirs), they should go home with a tan and a bunch of unforgettable experiences.</p>
                    <p>How much money should they bring? As little as possible. But yes, we do know they may want to bring home some souvenirs, but please limit the amount they have with them as neither we, nor the hotel will be held responsible in case they lose track of their cash. We figure around $100 (or its equivalent) should be more than enough.</p>
                    <p>Oh, and please don’t forget the chargers!</p>

                    <h2>DROP OFFS AND PICK UPS</h2>
                    <ul>
                        <li>
                            <b>Day 1 – Start of Teen Camp, Kids drop-off</b><br>
                            April 2, 2015 – The official venue of MyEO Next Gen Entrepreneurs Teen Camp is at the Sofitel, Philippine Plaza Hotel. Please drop-off kids early with all their stuff for the 4-day camp as Check-in and Registration starts at 7:00AM, Palawan Room (TBC), Mezzanine Floor, Sofitel.
                        </li>

                        <li><p>Then leave them in our able, loving hands. That should be the last time you will be seeing them until...</p></li>

                        <li>
                            <b>Day 4 – End of Teen Camp </b><br>
                            <p>April 5, 2015 – 7pm, Awards Night at The Henry Hotel, Pasay.<br>
                            (Address: 2680 FB Harrison St. Pasay City, 1300 Philippines T: +63 2 807 8888)</p>
                            <p>Special note: For delegates (foreign and local) staying at the hotel until April 6, they can leave their baggage at Sofitel.</p>
                        </li>
                    </ul>
                    <p>Someone from EO will get in touch with you to confirm your attendance at this event as we have limited seats. Please let us know right away so we can assure you a spot in this barbeque!</p>

                    <h2>STILL WORRIED?</h2>
                    <p>Don’t be!</p>
                    <p>In case of emergency, we will know how to get in touch with you – PROMISE.</p>
                    <ul>
                        <li><p>If your teen has special dietary requests/restrictions and other needs: please email us at inquiries@myeonextgen.com.</p></li>
                        <li><p>We know that it is Holy Week, so those Catholics who are concerned about Easter Sunday Mass, we have arranged with the hotel for your teens to attend the service at 7am. The schedule is very packed, but it is important, so we have been sure to squeeze it in. Please drop us a message in case your child needs reminding.</p></li>
                    </ul>
                    <p>Still curious about what they’re up to? Check out #nextgenmanila on Twitter, or check out the website for updates.</p>
                    <p>Ok, so we weren’t entirely truthful about seeing your teens ONLY on Sunday evening (we just needed you to get used to the idea of leaving them alone with us).</p>
                    <p>We do have some socials which are open to parents & guests:</p>
                    <div class="reminders__card">
                      <p class="reminders__card--header">The MyEO Next Gen Manila EXPO!</p>
                      April 5, 2015 (Day 4), from 10-1130AM, at the Harbor Deck, Sofitel Philippine Plaza
                    </div>

                    <p>Finally, you get a chance to see what your kids have been up to all this time. But they do still have a whole afternoon of other stuff they need to do before Awards Night, so we’ll see you again later.</p>
                    <p>We think we’ve got it covered. So to assure you all, we are posting all the important names and numbers you need to know.</p>

                    <h2>Organizers Contact Details</h2>
                    <ul>
                        <li>
                            <p><b>Carlo Buenaflor</b><br>
                            MyEO Next Gen Chairperson <br>
                            Mobile: 0917 558 2444 <br>
                            E-mail: inquiries@myeonextgen.com<br>
                            </p>
                        </li>
                        <li>
                            <p><b>Mia Silva </b><br>
                            EO Chapter Manager <br>
                            Mobile: 0917 599 0441 <br>
                            E-mail: miasilva226@gmail.com<br>
                            </p>
                        </li>
                        <li>
                            <p><b>Daphne Padasas </b><br>
                            EO Admin Assistant <br>
                            Mobile: 0926 930 2660 <br>
                            E-mail: padaphne49@gmail.com<br>
                            </p>
                        </li>
                        <li>
                            <p><b>Tanya Llana </b><br>
                            Event Coordinator <br>
                            Mobile: 0917 507 8140 <br>
                            E-mail: Tanya@hotellacoronalipa.com<br>
                            </p>
                        </li>
                    </ul>

                    <p>Now that’s really it. Piece of cake. Your teens are in EXCELLENT hands. Now will you stop worrying?</p>
                    <p>We’ll let your teens speak for themselves at the end of the camp so you can believe for yourselves.</p>
                    <p>Until then, try not to let the teens get too excited so we can have fun building it up for them.</p>
                    <p>We’ll see you there! Signed,</p>

                    <img src="<?php echo get_template_directory_uri() ?>/assets/images/signature.jpg" class="reminders__signature" />
                    <p>Carlo B.</p>

                    <div class="reminders__footer">
                        <img src="<?php echo get_template_directory_uri() ?>/assets/images/transformation.jpg"/>
                    </div>
                </div>
            </div>

        <?php endwhile; else : ?>


            <!-- The very first "if" tested to see if there were any Posts to -->
            <!-- display.  This "else" part tells what do if there weren't any. -->
            <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>

        <!-- REALLY stop The Loop. -->
        <?php endif; ?>
    </div>
 <?php get_footer(); ?>