<?php
/**
 * Next Gen EO functions and definitions
 *
 * When using a child theme (see http://codex.wordpress.org/Theme_Development and
 * http://codex.wordpress.org/Child_Themes), you can override certain functions
 * (those wrapped in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before the parent
 * theme's file, so the child theme functions would be used.
 *
 * @package Next Gen EO
 * @since 0.1.0
 */

 // Useful global constants
define( 'NGE_VERSION', '0.1.0' );

 /**
  * Set up theme defaults and register supported WordPress features.
  *
  * @uses load_theme_textdomain() For translation/localization support.
  *
  * @since 0.1.0
  */
 function nge_setup() {
	/**
	 * Makes Next Gen EO available for translation.
	 *
	 * Translations can be added to the /lang directory.
	 * If you're building a theme based on Next Gen EO, use a find and replace
	 * to change 'nge' to the name of your theme in all template files.
	 */
	load_theme_textdomain( 'nge', get_template_directory() . '/languages' );
 }
 add_action( 'after_setup_theme', 'nge_setup' );

 /**
  * Enqueue scripts and styles for front-end.
  *
  * @since 0.1.0
  */
 function nge_scripts_styles() {
	$postfix = ( defined( 'SCRIPT_DEBUG' ) && true === SCRIPT_DEBUG ) ? '' : '.min';

	wp_enqueue_script( 'nge', get_template_directory_uri() . "/assets/js/next_gen_eo{$postfix}.js", array(), NGE_VERSION, true );
	wp_enqueue_style( 'nge', get_template_directory_uri() . "/assets/css/next_gen_eo{$postfix}.css", array(), NGE_VERSION );
 }
 add_action( 'wp_enqueue_scripts', 'nge_scripts_styles' );

 /**
  * Add humans.txt to the <head> element.
  */
 function nge_header_meta() {
	$humans = '<link type="text/plain" rel="author" href="' . get_template_directory_uri() . '/humans.txt" />';

	echo apply_filters( 'nge_humans', $humans );
 }
 add_action( 'wp_head', 'nge_header_meta' );








function register_nge_menu() {
    register_nav_menu('header_menu',__( 'Header Menu' ));
}
add_action( 'init', 'register_nge_menu' );


/**
  * Add li.underline to the main nav
  */
add_filter( 'wp_nav_menu_items', 'menu_underline', 10, 2 );
function menu_underline ( $menu, $args ) {
    if ( $args->theme_location == 'header_menu') {
        $menu .= '<li class="underline"></li>';
    }
    return $menu;
}


/**
 * Itinerary shortcode
 * e.g. [itinerary_blocks img="image link" day="2" day_name="Friday" date="April 2"]
 */
function it_blocks( $atts, $content = null ) {
    $a = shortcode_atts( array(
        'img' => '',
        'day' => '',
        'day_name' => '',
        'date' => ''
    ), $atts );

    return
    "<div class='block one-fourth itinerary__block' data-day='". $a['day'] ."'>".
      "<div class='inner__block'>".
        "<img src='" . $a['img'] . "' />".
        "<div class='itinerary__copy'>".
          "<span class='day'>Day ". $a['day'] ."</span>".
          "<span class='day-name'>". $a['day_name'] ."</span>".
          "<span class='date'>". $a['date'] ."</span>".
        "</div>".
      "</div>".
    "</div>".
    '<div class="lightbox" id="day-' . $a['day'] . '" data-day="'. $a['day'] .'">'.
      '<div class="lightbox__modal">'.
        '<div class="block__container">'.
          $content .
        '</div>'.
        '<button class="close-modal"></button>'.
      '</div>'.
    '</div>';
}
add_shortcode( 'itinerary_blocks', 'it_blocks' );


/*
 * FAQ shortcode
 * e.g. [faq_block img="image url" title="Hello World" ][/faq_block]
 */
function faq_block( $atts, $content = null ) {
    $a = shortcode_atts( array(
        'img' => '',
        'title' => '',
        'target' => ''
    ), $atts );

    return
    '<div class="block one-third faq__block" data-target="#'. $a['target'] .'">'.
      '<div class="faq__inner-block">'.
        '<h2>'. $a['title'] .'</h2>'.
        '<div class="faq__image"><img src="'. $a['img'] .'" alt="'. $a['title'] .'"/></div>'.
      '</div>'.
    '</div>'.
    '<div class="lightbox" id="' . $a['target'] . '">'.
      '<div class="lightbox__modal">'.
        '<div class="block__container">'.
          '<div class="block one-third">'.
            '<h2 class="lb-header">' . $a['title'] . '</h2>'.
            '<img src="'. $a['img'] .'" alt="'. $a['title'] .'" class="lb-image" />'.
          '</div>'.
          '<div class="block two-thirds">'. $content . '</div>'.
        '</div>'.
        '<button class="close-modal"></button>'.
      '</div>'.
    '</div>';
}
add_shortcode( 'faq_block', 'faq_block' );


//Page Slug Body Class
function add_slug_body_class( $classes ) {
    global $post;
    if ( isset( $post ) ) {
        $classes[] = $post->post_type . '-' . $post->post_name;
    }
    return $classes;
  }
add_filter( 'body_class', 'add_slug_body_class' );