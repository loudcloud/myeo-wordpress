<?php
/**
 * The itinerary template file
 * Template Name: FAQ
 *
 * @package Next Gen EO
 * @since 0.1.0
 */

 get_header(); ?>

    <div class="faq__container faq">
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

            <div class="block__container">
                <div class="block one-third faq__block">
                  <div class="faq__inner-block">
                    <h1>FAQ</h1>
                    <p>Frequently Asked Questions</p>
                    <div class="faq__image"><img src="<?php echo get_template_directory_uri() ?>/assets/images/faq/faq.png" alt="Frequently Asked Questions"/></div>
                  </div>
                </div>
                
                <?php the_content(); ?>
            </div>

        <?php endwhile; else : ?>


            <!-- The very first "if" tested to see if there were any Posts to -->
            <!-- display.  This "else" part tells what do if there weren't any. -->
            <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>

        <!-- REALLY stop The Loop. -->
        <?php endif; ?>
    </div>
 <?php get_footer(); ?>