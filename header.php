<?php
/**
 * The template for displaying the header.
 *
 * @package Next Gen EO
 * @since 0.1.0
 */
 ?>
 <!DOCTYPE html>
    <html>
        <head>
            <meta charset="utf-8">

            <!-- Always force latest IE rendering engine or request Chrome Frame -->
            <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <title><?php bloginfo('name'); ?></title>
            <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic' rel='stylesheet' type='text/css'>
            <link href="<?php echo get_stylesheet_directory_uri() ?>/assets/css/sass/normalize.css" rel="stylesheet" type="text/css">
            <?php wp_head(); ?>
        </head>
        <body <?php body_class(); ?> >
        <?php if (!is_front_page()) : ?>
            <nav class="main-nav">
                <button class="mobile-menu js--mobile-nav"><span></span>Menu</button>
                <a href="<?php echo home_url(); ?>" class="logo">MyEO Next Gen Manila</a>
                <?php
                    $args = array(
                        'theme_location'  => 'header_menu',
                        'container'       => false,
                        'container_id'    => '',
                        'menu_class'      => 'menu',
                        'menu_id'         => '',
                        'echo'            => true,
                        'fallback_cb'     => 'wp_page_menu',
                        'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>'
                    );
                ?>
                <?php wp_nav_menu( $args ); ?>
            </nav>
        <?php endif; ?>