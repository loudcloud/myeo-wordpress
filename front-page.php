<?php
/**
 * The landing page of my next gen eo
 * Template Name: Landing Page
 * @package Next Gen EO
 * @since 0.1.0
 */
get_header();
$learn = get_page_by_title( 'Learn More' );
$reminders = get_page_by_title( 'Reminders' );
$itinerary = get_page_by_title( 'Itinerary' );
$faq = get_page_by_title( 'FAQ' );

?>

<div class="landing-page__container block__container">
  <div class="block one-half landing-page__left">
    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/landing-my-eo.jpg" title="MY EO Next Gen Manila" class="landing-page__logo" />
    <div class="landing-page__copy">
      <p>Give your teens an eye-opening, mind-blowing camp experience this spring break!</p>

      <a href="<?php echo get_page_link($learn->ID) ?>" class="learn-more"><span>Learn More</span></a>
    </div>
  </div>

  <div class="block one-half">
    <div class="landing-page__links">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/images/landing-page-elements.jpg" class="base" >
      <a href="http://eoaccess.eonetwork.org/Malaysia/Events/Pages/EventDetails.aspx?eID=52549" class="contact-image"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/contact.jpg" title="Register" ></a>
      <a href="<?php echo get_page_link($itinerary->ID) ?>" class="itinerary-image"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/itinerary.jpg" title="Itinerary" ></a>
      <a href="<?php echo get_page_link($reminders->ID) ?>" class="reminders-image"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/reminders.jpg" title="Remidners" ></a>
      <a href="<?php echo get_page_link($faq->ID) ?>" class="faq-image"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/faq.jpg" title="FAQs" ></a>
    </div>
  </div>
</div>

<?php
get_footer();
?>