 <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

    <div class="block__container">
        <h2><a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
        <?php the_content(); ?>
    </div>

 <?php endwhile; else : ?>


    <!-- The very first "if" tested to see if there were any Posts to -->
    <!-- display.  This "else" part tells what do if there weren't any. -->
    <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>


    <!-- REALLY stop The Loop. -->
 <?php endif; ?>