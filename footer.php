<?php
/**
 * The template for displaying the footer.
 *
 * @package Next Gen EO
 * @since 0.1.0
 */?>
    <?php if (!is_front_page()) : ?>
      <div class="main-footer">
        <div class="block__container">
            <div class="block two-thirds">
              <img src="<?php echo get_template_directory_uri(); ?>/assets/images/footer.png" class="footer__img">
            </div>
            <div class="block one-third">
              <img src="<?php echo get_template_directory_uri(); ?>/assets/images/my-eo-ondark.png" class="footer__img-inline">
              <img src="<?php echo get_template_directory_uri(); ?>/assets/images/eo-ph.jpg" class="footer__img-inline inspire">
              <a href="<?php echo home_url(); ?>">myeonextgen.com</a>
              <a href="http://myeonetwork.org">myeonetwork.org</a>
              <a href="#">#nextgenmanila</a>
              <p class="copyright">Copyright All Rights Reserved &copy; <?php echo date("Y") ?></p>
            </div>
        </div>
      </div>
      <?php endif; ?>
    <?php wp_footer(); ?>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
    <script src="<?php echo get_template_directory_uri() ?>/assets/js/src/fittext.js" type="text/javascript"></script>
    <script type="text/javascript">
      $('document').ready(function(){
        $(".info__block h2").fitText();

        $('.close-modal').on('click', close_modal);
        $('.faq__block').on('click', function(){
          $('body').addClass("show--modal");
          $($(this).data('target')).addClass("show--modal");
        });

        $(document).keyup(function(e) {
          if (e.keyCode == 27) { close_modal(); }   // esc
        });

        $('.itinerary__block').on('click', function(){
          show_itinerary($(this).data('day'));
        });

        $('.js--next_day').on('click', function(){
          var next_day = +$(this).parents('.lightbox').data('day') + 1;
          console.log(next_day);
          show_itinerary(next_day);
        });

        $('.js--previous_day').on('click', function(){
          var previous_day = +$(this).parents('.lightbox').data('day') - 1;
          if (previous_day < 1) {
            previous_day = 1;
          }
          console.log(previous_day);
          show_itinerary(previous_day);
        });

        $('.js--mobile-nav').on('click', function(){
          var mobMenu = $('.main-nav ul');
          $(this).toggleClass('js--clicked');
          mobMenu.toggleClass('show--menu');
        })
      });

      function close_modal() {
        $('body').removeClass("show--modal");
        $('.lightbox').removeClass("show--modal");
      }

      function show_itinerary(day) {
        close_modal();
        $('body').addClass("show--modal");
        $('#day-' + day).addClass("show--modal");
      }
    </script>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-29314231-4', 'auto');
      ga('send', 'pageview');

    </script>
    </body>
</html>